require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

	def setup
		@user = users(:michael)
		@unactivated_user = users(:non_activated_user)
	end

	test "unsuccessful edit" do
		log_in_as(@user)
		get edit_user_path(@user)
		assert_template 'users/edit'
		patch user_path(@user), params: { user: { name: "",
																							email: "foo@invalid",
																							password:	"foo",
																							password_confirmation: "bar" } }
		assert_template 'users/edit'
		assert_select "div.alert", "The form contains 3 errors"
	end

	test "successful edit with friendly forwarding" do
		get edit_user_path(@user)
		log_in_as(@user)
		assert_redirected_to edit_user_url(@user)
		log_in_as(@user)
		assert_redirected_to user_path(@user)
		name = "Foo Bar"
		email = "foo@bar.com"
		patch user_path(@user), params: { user: { name: name,
																							email: email,
																							password: "",
																							password_confirmation: "" } }
		assert_not flash.empty?
		assert_redirected_to @user
		@user.reload
		assert_equal name, @user.name
		assert_equal email, @user.email
	end

	test "unsuccessful edit of unactivated user" do
		log_in_as(@user)
		get user_path(@unactivated_user)
		assert_redirected_to root_url
	end
end
